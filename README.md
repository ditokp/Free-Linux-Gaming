# About

**Free Linux Gaming** is a Peertube channel that showing gameplay video without commentary. The gameplay duration is up to 10 minutes, and sometimes more than 10 minutes.
This repository provide video editing file (.blend) so you can edit the video.
You can download the videos (both raw and final video) at Mega link in Peertube video description.

# FAQ
**Q**: "Can you make gameplay video more than 10 minutes?" / "Why you only make gameplay video in/under 10 minutes?"

**A**: "Maybe, I can, if the game is 2D pixel art." / "Because my potato laptop can't record video while playing game with stable FPS. It's so laggy."


**Q**: "Why you not uplouding the video on YouTube?"

**A**: "Because I more like Peertube. Because Peertube is free, open-source, no ads."

# License
All videos (both raw and final after edit), the .blend file, the .osp file and this repository is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License (CC by-SA 4.0)</a>.